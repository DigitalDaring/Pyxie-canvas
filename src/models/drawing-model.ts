import {LayerModel} from "./layer-model";

export class DrawingModel {
  name: string;
  zoom: number;
  layers: Array<LayerModel>;
}
