import {ColumnModel} from "./column-model";

export class LayerModel {
  name: string;
  priority: number;
  columns: Array<ColumnModel>;
}
