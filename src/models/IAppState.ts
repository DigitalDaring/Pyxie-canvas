import {DrawingModel} from "./drawing-model";

export interface IAppState {
  drawing: DrawingModel;
}
