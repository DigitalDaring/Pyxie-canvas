import {CellModel} from "./cell-model";

export class ColumnModel {
  rows: Array<CellModel>;
}
