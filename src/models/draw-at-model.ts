import {CellModel} from "./cell-model";

export class DrawAtModel {
  draw: CellModel;
  atX: number;
  atY: number;
  atZ: number;
}
