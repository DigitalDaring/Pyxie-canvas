import {Action} from "@ngrx/store";
import {DrawAtModel} from "../models/draw-at-model";


export const DrawingActions = {
  ADD_PIXEL: "ADD_PIXEL",
  GET_PIXELS: "GET_PIXELS"
}

export class AddPixel implements Action {
  readonly type = DrawingActions.ADD_PIXEL;
  payload: DrawAtModel;
}

export class GetPixels implements Action {
  readonly type = DrawingActions.GET_PIXELS;
}

