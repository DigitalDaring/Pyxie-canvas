import {DrawingModel} from "../models/drawing-model";
import {Action} from "@ngrx/store";
import {AddPixel, DrawingActions} from "../actions/drawing.actions";
import {CellModel} from "../models/cell-model";
import {LayerModel} from "../models/layer-model";

const c = {
  color: {
    red: 255,
    green: 255,
    blue: 255,
    opacity: 255
  }
} as CellModel;
const defaultState = {
  layers: [
    {
      columns: [
        {rows: [c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c]},
        {rows: [c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c]},
        {rows: [c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c]},
        {rows: [c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c]},
        {rows: [c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c]},
        {rows: [c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c]},
        {rows: [c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c]},
        {rows: [c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c]},
        {rows: [c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c]},
        {rows: [c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c]},
        {rows: [c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c]},
        {rows: [c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c]},
        {rows: [c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c]},
        {rows: [c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c]},
        {rows: [c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c]},
        {rows: [c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c]}
      ]
    } as LayerModel
  ],
  name: "",
  zoom: 16
} as DrawingModel;

export function DrawingReducer(state = defaultState, action: Action) {
  switch (action.type) {
    case DrawingActions.ADD_PIXEL: {
      const drawAtAction = action as AddPixel;

      if (state.layers.length > drawAtAction.payload.atZ
        && state.layers[drawAtAction.payload.atZ].columns.length > drawAtAction.payload.atX
        && state.layers[drawAtAction.payload.atZ].columns[drawAtAction.payload.atX].rows.length > drawAtAction.payload.atY
      ) {
        state.layers[drawAtAction.payload.atZ]
          .columns[drawAtAction.payload.atX]
          .rows[drawAtAction.payload.atY] = drawAtAction.payload.draw;
      }
      return Object.assign({}, state);
    }
  }
}
