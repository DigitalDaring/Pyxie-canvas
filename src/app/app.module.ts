import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { DrawingComponent } from "./drawing/drawing.component";
import {StoreModule} from "@ngrx/store";
import {DrawingReducer} from "../reducers/drawing.reducers";

@NgModule({
  declarations: [
    AppComponent,
    DrawingComponent
  ],
  imports: [
    BrowserModule,
    StoreModule.forRoot({
      drawing: DrawingReducer
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
