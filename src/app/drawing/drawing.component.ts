import { Component, OnInit } from "@angular/core";
import {IAppState} from "../../models/IAppState";
import {Store} from "@ngrx/store";
import {AddPixel} from "../../actions/drawing.actions";
import {DrawAtModel} from "../../models/draw-at-model";
import {CellModel} from "../../models/cell-model";

@Component({
  selector: "app-drawing",
  templateUrl: "./drawing.component.html",
  styleUrls: ["./drawing.component.css"]
})
export class DrawingComponent implements OnInit {

  canvas: any;
  currentContext: any;
  touchInterval: any;
  constructor(private store: Store<IAppState>) { }

  ngOnInit() {

    this.canvas = document.getElementById("drawing-canvas");
    this.currentContext = this.canvas.getContext("2d");

    this.store.select(state => state.drawing)
      .subscribe((updates) => {
        if (updates) {
          this.currentContext.clearRect(0, 0, this.canvas.width, this.canvas.height);
          updates.layers.forEach((layer) => {
            layer.columns.forEach((column, columnIndex) => {
              column.rows.forEach((cell, rowIndex) => {
                this.drawCell(columnIndex, rowIndex, 0, cell, updates.zoom);
              });
            });
          });
          this.drawGuideLines(updates.zoom);
        }
      });

  }

  drawCell(x: number, y: number, z: number, cell: CellModel, zoom: number) {
    this.currentContext.fillStyle = `rgba(${cell.color.red},${cell.color.green},${cell.color.blue},${cell.color.opacity})`;
    const xOffset = x * zoom;
    const yOffset = y * zoom;
    this.currentContext.fillRect(xOffset, yOffset, zoom, zoom);
  }

  drawGuideLines(zoom) {
    this.currentContext.fillStyle = `rgba(120,120,120,255)`;

    for (let x = zoom; x < this.canvas.width; x += zoom) {
      this.currentContext.fillRect(x, 0, 1, this.canvas.height);
    }

    for (let y = zoom; y < this.canvas.height; y += zoom) {
      this.currentContext.fillRect(0, y, this.canvas.width, 1);
    }

  }

  touch(event) {
    const zoom = 16;
    let x = event.clientX - event.srcElement.offsetLeft;
    let y = event.clientY - event.srcElement.offsetTop;
    x = x < 0 ? 0 : x;
    y = y < 0 ? 0 : y;
    const xOffset = Math.floor(x / zoom);
    const yOffset = Math.floor(y / zoom);

    const drawAt = {
      atY: yOffset,
      atX: xOffset,
      atZ: 0,
      draw: {
        color: {
          red: 0,
          green: 0,
          blue: 0,
          opacity: 255
        }
      } as CellModel
    } as DrawAtModel;

    const dispatchMe = new AddPixel();
    dispatchMe.payload = drawAt;

    this.store.dispatch(dispatchMe);
  }
}
